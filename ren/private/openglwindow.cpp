#include "../public/openglwindow.hpp"
#include "../public/openglstatemanager.hpp"
#include "../public/app.hpp"
#include "glad/glad.h"
#include "SDL.h"

namespace ren {

  struct OpenGLWindow::impl {
    App* app;
    SDL_Window* win = nullptr;
    SDL_Renderer* renderer = nullptr;
    SDL_GLContext context;
  };

  OpenGLWindow::OpenGLWindow(App* app)
    : m_impl(new impl)
  {
    m_impl->app = app;
  }

  void OpenGLWindow::create(const WindowParams& params) {
    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(params.width, params.height, SDL_WINDOW_OPENGL, &m_impl->win, &m_impl->renderer);
    m_impl->context = SDL_GL_CreateContext(m_impl->win);

    if (!gladLoadGL()) {
      fprintf(stderr, "Failed to load GL\n");
    }

    m_stateManager = new OpenGLStateManager;
  }

  void OpenGLWindow::run() {
    while (true) {
      SDL_Event evt;
      while (SDL_PollEvent(&evt)) {
        if (evt.type == SDL_QUIT || (evt.type == SDL_KEYDOWN && evt.key.keysym.scancode == SDL_SCANCODE_ESCAPE)) {
          return;
        }
      }

      m_impl->app->render();
      SDL_GL_SwapWindow(m_impl->win);
    }
  }

  OpenGLWindow::~OpenGLWindow() {
    delete m_stateManager;

    SDL_GL_DeleteContext(m_impl->context);
    SDL_DestroyRenderer(m_impl->renderer);
    SDL_DestroyWindow(m_impl->win);
  }

}