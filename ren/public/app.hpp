#pragma once

namespace ren {

  struct App {
    virtual ~App() {}
    virtual void render() = 0;
  };

}