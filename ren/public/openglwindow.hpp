#pragma once

#include "window.hpp"
#include <memory>

namespace ren {

  class StateManager;

  class OpenGLWindow : public Window {
  public:
    OpenGLWindow(App* app);
    ~OpenGLWindow();

    void create(const WindowParams& params) override;
    void run() override;
    
    StateManager* stateManager() override { return m_stateManager; };

  private:
    struct impl;
    std::unique_ptr<impl> m_impl;
    ren::StateManager* m_stateManager = nullptr;
  };

}