#pragma once

#include <unordered_map>
#include <typeindex>
#include <stack>
#include <type_traits>
#include <memory>

namespace ren {
  
  namespace internal {
    struct State {};
  }

  struct ClearColor : internal::State {
    ClearColor(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}
    bool operator==(const ClearColor& other) const { return r == other.r && b == other.b && g == other.g && a == other.a; }
    float r, g, b, a;
  };

  class StateManager {
  public:
    StateManager() {
      initState(ClearColor(0.0f, 0.0f, 0.0f, 0.0f));
    }

    virtual ~StateManager() {}

    template <typename T>
    void pushState(const T& state)
    {
      static_assert(std::is_base_of<internal::State, T>::value, "T must be a state type");
      auto* stack = static_cast<std::stack<T>*>(m_stateStacks[typeid(T)].get());
      if (!(stack->top() == state))
        apply(state);
      stack->push(state);
    }

    template <typename T>
    void popState()
    {
      static_assert(std::is_base_of<internal::State, T>::value, "T must be a state type");
      static_cast<std::stack<T>*>(m_stateStacks[typeid(T)].get())->pop();
    }

    // Virtual apply functions - implement these for your renderer
    virtual void apply(const ClearColor& c) = 0;

  private:
    // Call from constructor to initialise a state, undefined behavior if a state
    // is pushed or popped before this is called..
    template <typename T>
    void initState(const T& state) {
      static_assert(std::is_base_of<internal::State, T>::value, "T must be a state type");
      auto stack = std::make_shared<std::stack<T>>();
      stack->push(state);
      m_stateStacks[typeid(T)] = stack;
    }

    std::unordered_map<std::type_index, std::shared_ptr<void>> m_stateStacks;
  };

}