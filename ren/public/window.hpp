#pragma once

namespace ren {

  struct App;
  class StateManager;

  static struct WindowParams {
    const char* title = "hello world";
    int width = 1024;
    int height = 1024;
  } defaultParams;

  struct Window {
    virtual ~Window() {}
    virtual void create(const WindowParams& params) = 0;
    virtual void run() = 0;
    virtual StateManager* stateManager() = 0;
  };

}