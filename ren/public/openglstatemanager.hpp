#pragma once

#include "statemanager.hpp"
#include "glad/glad.h"

namespace ren {

  struct OpenGLStateManager : public StateManager {
    void apply(const ClearColor& c) override {
      glClearColor(c.r, c.g, c.b, c.a);
    }
  };

}