#include <iostream>
#include <SDL.h>

#include <app.hpp>
#include <statemanager.hpp>
#include <openglwindow.hpp>

#include "glad/glad.h"

class TestApp : public ren::App {
public:
  void run();
  void init();
  void render();

private:
  std::unique_ptr<ren::Window> m_win;
};

int main(int argc, char** argv) {
  TestApp app;
  app.run();
  return 0;
}

void TestApp::run() {
  m_win = std::make_unique<ren::OpenGLWindow>(this);
  m_win->create(ren::defaultParams);
  init();
  m_win->run();
}

void TestApp::init() {
}

void TestApp::render() {
  m_win->stateManager()->pushState(ren::ClearColor(1.0f, 0.0f, 1.0f, 1.0f));
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  float buf[] = {
    0.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f
  };
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), buf);
  glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), buf);
  glDrawArrays(GL_TRIANGLES, 0, 3);
  m_win->stateManager()->popState<ren::ClearColor>();
}