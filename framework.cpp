#include <stdio.h>
#include <string.h>
#include <memory>
#include "SDL.h"
#include "SDL_vulkan.h"
#include "vulkan.h"

struct VulkanContext
{
  VkInstance instance;
  VkDevice device;
  VkSurfaceKHR surface;
  VkSwapchainKHR swapchain;
  VkPhysicalDeviceProperties physicalDeviceProperties;
  VkPhysicalDeviceFeatures physicalDeviceFeatures;
  uint32_t graphicsQueueFamilyIndex;
  uint32_t presentQueueFamilyIndex;
  VkPhysicalDevice physicalDevice;
  VkQueue graphicsQueue;
  VkQueue presentQueue;
  VkSemaphore imageAvailableSemaphore;
  VkSemaphore renderingFinishedSemaphore;
  VkSurfaceCapabilitiesKHR surfaceCapabilities;
  uint32_t surfaceFormatsAllocatedCount;
  uint32_t surfaceFormatsCount;
  uint32_t swapchainDesiredImageCount;
  VkSurfaceFormatKHR surfaceFormat;
  VkExtent2D swapchainSize;
  VkCommandPool commandPool;
  uint32_t swapchainImageCount;
  VkImage* swapchainImages;
  VkCommandBuffer* comandBuffers;
  VkFence* fences;
};

int main(int argc, char** argv)
{
  VulkanContext ctx = {};

  // Init vulkan
  {
    SDL_Vulkan_LoadLibrary(nullptr);

    // Load functions
  }

  // Clean up vulkan
  {
    SDL_Vulkan_UnloadLibrary();
  }

  return 0;
}